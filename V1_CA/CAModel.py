import numpy as np
import argparse

# Cell States:
#   __ Killed : B0 Microbes that were killed by drug administration during the previous step
#   00 Empty
#   01 White blood cell (WBC)
#   10 Bacteria Level 0 (B0): can be killed by WBCs 
#   11 Bacteria Level 1 (B1): cannot be killed by WBCs
#   12 Bacteria Level 2 (B2): cannote be killed by WBCs and can protect its Moore neighbors from be killed by WBCs

#
# CAModel(MVC -- Model part)
#   Dealing with the system data and logic.
#   Computational result at each step will be provide via data() function
class CAModel:
    def __init__(self):
        self.CA_world_size = (200,200)
        self.CA_world_total_cell = self.CA_world_size[0] * self.CA_world_size[1]
        self.Cell_State = {
            'Killed' : 0,
            "Empty": 1,
            "WBC": 2,
            "B0": 3,
            "B1": 4
        }
        self.count_types = ["B0", "B1"]

        self.main_data = np.ones(self.CA_world_size, dtype='i')
        # save 10 steps in order to check for life span
        self.main_data_history = [self.main_data.copy()] * 10
        
        self.step = 0

        self.population_statistics = None
        self.population_derivative = None

        # Chance for a microbe to undergo mutation: this should be really small
        self.mutation_rate = .00
        self.beneficial_rate = .01
        # Chance for growth of microbe 
        self.growth_rate = .8

        # Relative strength of the immune system
        self.immune_strength = 80

        # Frequency the drug is administered
        self.drug_regiment = 5

        # How long can the bacteria grow for before adminstering medicine?
        self.incubation_time = 25

        # Bacteria Life Span
        self.bac_life_span = False

        self.frame_limit = 150


        # example intial bacteria
        self.main_data[int(self.CA_world_size[0]/2), int(self.CA_world_size[1]/2)] = self.Cell_State["B0"]

        self.parse_arguments()

    def parse_arguments(self):
        # === Start parsing arguments from command line ===
        parser = argparse.ArgumentParser()
        parser.add_argument("-m", "--mutation_rate", help="Chance for a microbe to undergo mutation. try: 0.00", type=float)
        parser.add_argument("-b", "--beneficial_rate", help="Chance for a microbe survive during mutation. try: 0.01", type=float)
        parser.add_argument("-g", "--growth_rate", help="Chance for growth of microbe. try: 0.8", type=float)
        parser.add_argument("-i", "--immune_strength", help="Relative strength of the immune system. try: 80", type=float)
        parser.add_argument("-d", "--drug_regiment", help="Frequency the drug is administered. try: 5", type=float)
        parser.add_argument("-c", "--incubation_time", help="How long can the bacteria grow for before adminstering medicine? try: 25", type=float)
        parser.add_argument("-l", "--bac_life_span", help="Bacteria Life Span, try: 0 or 1", type=float)
        parser.add_argument("-f", "--frame_limit", help="frame_limit", type=int)
        args = parser.parse_args()
        if args.mutation_rate:
            self.mutation_rate = args.mutation_rate
        if args.beneficial_rate:
            self.beneficial_rate = args.beneficial_rate
        if args.growth_rate:
            self.growth_rate = args.growth_rate
        if args.immune_strength:
            self.immune_strength = args.immune_strength
        if args.drug_regiment:
            self.drug_regiment = args.drug_regiment
        if args.incubation_time:
            self.incubation_time = args.incubation_time
        if args.bac_life_span:
            self.bac_life_span = args.bac_life_span
        if args.frame_limit:
            self.frame_limit = args.frame_limit


    def Rule_life_span(self, new_data):
        # --Rule 1: life span
        # Rule logic express by numpy array
        # select any cell with "thing"s. selected will be an array with True and False.
        selected = self.main_data>0
        # if previous 10 steps are same with current step, means that "thing" lived for at least 10 steps.
        selected = np.logical_and.reduce( (
            selected,
            (self.main_data_history[0] == self.main_data),
            (self.main_data_history[1] == self.main_data),
            (self.main_data_history[2] == self.main_data),
            (self.main_data_history[3] == self.main_data),
            (self.main_data_history[4] == self.main_data),
            (self.main_data_history[5] == self.main_data),
            (self.main_data_history[6] == self.main_data),
            (self.main_data_history[7] == self.main_data),
            (self.main_data_history[8] == self.main_data),
            (self.main_data_history[9] == self.main_data)
        ))
        # set all those old "thing"s to die
        # If we can to give the bacteria a life span.
        if not self.bac_life_span:
            wbc = self.main_data == self.Cell_State['WBC']
            selected = np.logical_and.reduce((selected, wbc))
        new_data[selected] = self.Cell_State["Empty"]
        return new_data

    def Rule_bacteria_growth(self, new_data):
        # --Rule: bacteria give birth to the cell next to it
        # Rule logic express by numpy array
        parent_types = ["B0", "B1"]
        # grow in random order, so no species can have absolute advantage.
        random_orders = np.arange(len(parent_types))
        np.random.shuffle(random_orders)
        # every step bacteria has a probability of 0.1 to grow?
        random_growth = np.random.rand(self.main_data.shape[0], self.main_data.shape[1])<self.growth_rate # Can we make this a constant in the init?
        random_mutation = np.random.rand(self.main_data.shape[0], self.main_data.shape[1])<self.mutation_rate # Same here 
        beneficial_mutation = np.random.rand(self.main_data.shape[0], self.main_data.shape[1])<self.beneficial_rate # Maybe this should be it's own rate (beneficial mutation)
        # populate all bacteria types
        for random_order in random_orders:
            parent_type = parent_types[random_order]
            # select all empty cells
            empty_cell = self.main_data==self.Cell_State['Empty']
            # check Moore neighborhood
            b_0 = np.roll(self.main_data, 1, axis=0)==self.Cell_State[parent_type]
            b_1 = np.roll(self.main_data, -1, axis=0)==self.Cell_State[parent_type]
            b_2 = np.roll(self.main_data, 1, axis=1)==self.Cell_State[parent_type]
            b_3 = np.roll(self.main_data, -1, axis=1)==self.Cell_State[parent_type]
            # apply logic, np.logical_or.reduce is basically means do logical_or to all b0, b1, etc.
            has_neighbor = np.logical_or.reduce( (b_0,b_1,b_2,b_3) )
            selected = np.logical_and.reduce( (random_growth, empty_cell, has_neighbor) )
            # set all empty cell who has at least one neighbor to it's neighbor
            # This is where the white blood cell conversion should take place, when the cells care growing. 
            new_data[selected] = self.Cell_State[parent_type]

            new_data = self.Rule_wbc_occurs(new_data, selected)
            # mutate to +1 level
            if (parent_type != "B1"):
                beneficial_mutation = np.logical_and.reduce( (selected, random_mutation, beneficial_mutation) )
                harmful_mutation = np.logical_and.reduce( (selected, random_mutation, ~beneficial_mutation) )
                new_data[beneficial_mutation] = self.Cell_State[parent_type] + 1
                new_data[harmful_mutation] = self.Cell_State['Empty']
        return new_data

    # def Rule_wbc_occurs(self, new_data):
    def Rule_wbc_occurs(self, new_data, new_growth):
        # --Rule: WBC occurs in empty cells due to random chance
        # empty_cell = self.main_data==0

        # The probability of the wbc grows as the number of bacteria in the system grow.
        # First term ensures prob of WBC < 1 
        probability_of_wbc = self.immune_strength * (+
        np.sum(self.main_data>=self.Cell_State["B0"]) / self.CA_world_total_cell)

        # Changed to only bring white blood cells to areas of new growth
        random_chance = np.random.rand(self.main_data.shape[0], self.main_data.shape[1])<probability_of_wbc
        selected = np.logical_and.reduce( (new_growth, random_chance) )
        new_data[selected] = self.Cell_State["WBC"]
        return new_data

    def Rule_wbc_kill_b0(self, new_data):
        # --Rule: WBC kills it's neighbor bacteria, but B2 protect its neighbor, B1 cannot be killed.
        # on the other words, B0 without B2 as its neighbor and with WBC as its neighbor die.
        B0_cell = self.main_data>=self.Cell_State["B0"]
        # check Von Neumann neighborhood
        wbc_0 = np.roll(self.main_data, 1, axis=0)==self.Cell_State["WBC"]
        wbc_1 = np.roll(self.main_data, -1, axis=0)==self.Cell_State["WBC"]
        wbc_2 = np.roll(self.main_data, 1, axis=1)==self.Cell_State["WBC"]
        wbc_3 = np.roll(self.main_data, -1, axis=1)==self.Cell_State["WBC"]
        # b2_0 = np.roll(self.main_data, 1, axis=0)!=self.Cell_State["B2"]
        # b2_1 = np.roll(self.main_data, -1, axis=0)!=self.Cell_State["B2"]
        # b2_2 = np.roll(self.main_data, 1, axis=1)!=self.Cell_State["B2"]
        # b2_3 = np.roll(self.main_data, -1, axis=1)!=self.Cell_State["B2"]
        selected = np.logical_or.reduce( (wbc_0, wbc_1, wbc_2, wbc_3) )
        # selected = np.logical_and.reduce( (selected, B0_cell, b2_0, b2_1, b2_2, b2_3) )
        selected = np.logical_and.reduce( (selected, B0_cell) )
        new_data[selected] = self.Cell_State["Empty"]
        return new_data

    # Method for adding in the drug 'waves'
    def Administer_Drug(self, new_data):
        # Find the number of microbes with < 4 neighbors, and kill all 'B0's that don't have those 
        # Mutated cells are protected from drug 

        # All cells that are susceptible to the drug
        non_susceptible = self.main_data != self.Cell_State["B0"]

        # Count the number of Moore Neighbors
        b2_0 = np.roll(self.main_data, 1, axis=0)>self.Cell_State["Empty"]
        b2_1 = np.roll(self.main_data, -1, axis=0)>self.Cell_State["Empty"]
        b2_2 = np.roll(self.main_data, 1, axis=1)>self.Cell_State["Empty"]
        b2_3 = np.roll(self.main_data, -1, axis=1)>self.Cell_State["Empty"]

        # Subtract the number of WBC from neighborhood
        wbc_0 = np.roll(self.main_data, 1, axis=0)!=self.Cell_State["WBC"]
        wbc_1 = np.roll(self.main_data, -1, axis=0)!=self.Cell_State["WBC"]
        wbc_2 = np.roll(self.main_data, 1, axis=1)!=self.Cell_State["WBC"]
        wbc_3 = np.roll(self.main_data, -1, axis=1)!=self.Cell_State["WBC"]

        # If there's >3 non-WBC neighbors, they're protected 
        protected_by_neighbors = np.logical_and.reduce( (b2_0, b2_1, b2_2, b2_3) )
        protected_by_neighbors = np.logical_and.reduce( (protected_by_neighbors, wbc_0, wbc_1, wbc_2, wbc_3) )
        protected = np.logical_or.reduce((protected_by_neighbors, non_susceptible))

        # Kill all others
        new_data[~protected] = self.Cell_State['Killed']
        return new_data

    # Any bacteria killed by the previous will be removed and set to empty
    def Remove_killed_bacteria(self, new_data):
        # Found those killed
        killed = self.main_data == self.Cell_State['Killed']
        # Remove them
        new_data[killed] = self.Cell_State['Empty']
        return new_data

    def data_generator(self):
        # Just some temperary example rules here. numpy operation should be considered, to make things faster.

        # Step 1. Create memory space for new data
        new_data = self.main_data.copy()

        # Should not have the killed bacteria influencing the population
        # It looks better in simulation if we simply skip the rest of the steps and only remove those 'killed'
        # If the previous step was a drug administration step, remove old bacteria
        if self.drug_regiment is not None and self.step  > self.incubation_time and (self.step - 1)%self.drug_regiment == 0:
            new_data = self.Remove_killed_bacteria(new_data)
        # Step 2b: Adminster the drug dependent on the regiment rate
        elif self.drug_regiment is not None and self.step  > self.incubation_time and self.step%self.drug_regiment == 0:
            new_data = self.Administer_Drug(new_data)
        else:
            # Step 2. Modify new data based on rules of each step. (We should avoid for-loop here as much as possible.)
            new_data = self.Rule_life_span(new_data)

            # Bacterial growth 
            new_data = self.Rule_bacteria_growth(new_data)
            # new_data = self.Rule_wbc_occurs(new_data) # Removed this and set inside Rule_bacteria_growth() 
            new_data = self.Rule_wbc_kill_b0(new_data)

            
       
        # Step 3. Save current main data to history and update current main data
        for i in range(9,0,-1):
            self.main_data_history[i] = self.main_data_history[i-1]
        self.main_data_history[0] = self.main_data.copy()
        self.main_data = new_data

        # Step 4. Save population statistics
        ret = np.zeros(len(self.count_types)+1, dtype='i')
        ret[0] = self.step
        for index, count_type in enumerate(self.count_types):
            count = (self.main_data==self.Cell_State[count_type]).sum()
            ret[index+1] = count
        ret = ret.reshape(1,-1)
        if self.step>0:
            self.population_statistics = np.append( self.population_statistics, ret, axis=0 )
            self.population_derivative = np.append( self.population_derivative, ret-self.population_statistics[-2], axis=0 )
        else:
            self.population_statistics = ret
            self.population_derivative = np.zeros_like(ret, dtype='i')
        self.step += 1


if __name__ == "__main__":
    from CAView import CAView
    model = CAModel()
    view = CAView(model=model)
    view.show()
