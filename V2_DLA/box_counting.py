import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pickle
import os
import glob
import numpy_indexed as npi
from numba import jit

# numba.jit makes pure computation functions run way more faster
@jit(nopython = True)
def box_counting(main_data, epsilon):
    n = 0
    for i in range(0, main_data.shape[0], epsilon):
        for j in range(0, main_data.shape[1], epsilon):
            if np.sum(main_data[i:i+epsilon, j:j+epsilon])>=1:  #<- if there's something in the box:
                n += 1                                          #<- count
    return n

# loop over all data files produced by DLA simulations
fnames = glob.glob("data/dla_main_data_*.pickle")
for fname in fnames:
    with open(fname, "rb") as fp:
        main_data = pickle.load(fp)
    
    # Box size from 1 to n-1
    epsilons = [*range(1,8)] #<- make it a list
    box_numbers = []
    for epsilon in epsilons:
        box_numbers.append(box_counting(main_data, epsilon))

    # Find polynomial coefficients
    # y = coeffs[0]*x**degree + coeffs[1]*x**(degree-1) + ... + coeffs[-1]
    coeffs=-np.polyfit(np.log(epsilons), np.log(box_numbers), 1)

    print(fname, "Box counting dimension: ", coeffs[0])

    pngname = fname[:-5]+"-dimension.png"
    print(pngname)
    # if we want to see the plot for each file:
    fig = plt.figure()
    ax = plt.subplot(111)
    ax.scatter(epsilons, box_numbers)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_title('Box-Counting Dimension: %.3f'%coeffs[0])
    plt.savefig(pngname)
    plt.close()
    #plt.show()
    