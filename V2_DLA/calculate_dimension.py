import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pickle
import os
import glob
import numpy_indexed as npi

shift_r = []
correlation_densities = []

# read data from all 3001-particle results
files = glob.glob('data/dla_main_data_3001_*.pickle')
for fname in files:
    print(fname)
    f = open(fname, "rb")
    dla_main_data = pickle.load(f)
    
    total_particle = np.sum( dla_main_data>0 ) #<- should be 3001

    for distance_r in range(1,50): #<- shifting different distances
        for axis in range(2): #<- shifting towards different axis (directions)
            shifted_data = np.roll(dla_main_data, shift=distance_r, axis=axis)
            overlap = np.logical_and.reduce(((shifted_data>0),(dla_main_data>0)))

            n = np.sum(overlap)
            shift_r.append(distance_r)
            correlation_densities.append(n/total_particle)

# now we have shift_r and correlation_densities

fig, axes = plt.subplots(1,2)
# zip two list and make a boxplot, like the author did
data = zip(shift_r,correlation_densities)
data = np.array(list(data))
groups = npi.group_by(data[:,0])
data = groups.split(data[:,1])
xaxis = groups.unique
axes[0].boxplot(data.T, positions=xaxis)

#or simply use scatter
axes[1].scatter(shift_r, correlation_densities, s=0.4)

# set the plot to be the same scale as the author did
for i in range(2):
    axes[i].set_xlabel("r (Lattice Const.)")
    axes[i].set_ylabel("C(r)")
    axes[i].set_xlim([2,50])
    axes[i].set_ylim([0,0.4])
    axes[i].set_xscale('log')
    axes[i].set_xticks([2,5,10,20,50])
    axes[i].get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())

x = np.arange(1,50)
y = 0.43*x**(-0.343)
axes[1].plot(x,y)

plt.show()