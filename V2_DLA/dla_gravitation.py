import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import time, sys
import pickle
from numba import jit
from PIL import Image

dla_world_size = 1000

def init():
    global dla_main_data, graph_radius,c 
    dla_main_data = np.zeros((dla_world_size,dla_world_size), dtype='i')
    # Center picture
    # cs302 = (np.array(Image.open("resources/cs302.png"))==False)[:,:,0].T
    # cs302 = np.flip(cs302, axis=1)
    # startx = int((dla_world_size-cs302.shape[0])/2)
    # starty =  int((dla_world_size-cs302.shape[1])/2)
    # dla_main_data[startx:startx+cs302.shape[0], starty:starty+cs302.shape[1]] = cs302
    #plt.imshow(dla_main_data)
    #plt.show()
    #exit()
    # Center dot
    dla_main_data[int(dla_world_size/2)-100:int(dla_world_size/2)+100, int(dla_world_size/2)-100:int(dla_world_size/2)+100] = 1
    graph_radius = 1
    c = 0
init()


fig = plt.figure(figsize=[16,16])
main_image = plt.imshow(dla_main_data.T, cmap='terrain_r', origin='lower')

@jit(nopython=True)
def fast_compute(c, graph_radius, dla_main_data):
    ret_c = c
    ret_graph_radius = graph_radius
    ret_dla_main_data = dla_main_data

    if c%100==0:
        print("legal particle ", c, "; graph_radius ", graph_radius, "; ")

    end = 0
    while not end:
        # generate a new particle around the graph
        theta = np.random.rand()
        radius = 1 * graph_radius + 10 #<- new particle will be 1.1 * graph_radius + 10 away from the origin. graph_radius will be updated dynamically.
        x = int(radius * np.sin(theta*2*np.pi)+dla_world_size/2)
        y = int(radius * np.cos(theta*2*np.pi)+dla_world_size/2)

        for j in range(2000): #<- max step of random walk
            #dla_main_data[x,y] = 2
            # if touch boudary, abandon this particle
            if x<=1 or y<=1 or x>=dla_world_size-1 or y>=dla_world_size-1:
                break
            # if touch graph, grow
            if dla_main_data[x-1,y]==1 or dla_main_data[x,y-1]==1 or dla_main_data[x+1,y]==1 or dla_main_data[x,y+1]==1:
                ret_dla_main_data[x,y] = 1
                distance = np.linalg.norm(np.array([x-dla_world_size/2, y-dla_world_size/2]))
                if distance>graph_radius:
                    ret_graph_radius = distance
                ret_c += 1
                end = 1
                break
            # random walk
            r = np.random.randint(0,8) #<- instead of 0,4, introduce gravitational bias here
            if r==1:
                y+=1
            elif r==2:
                x+=1
            elif r==3:
                x-=1
            else:
                y-=1
    return ret_c, ret_graph_radius, ret_dla_main_data

def animate(args):
    global c, graph_radius, dla_main_data
    c, graph_radius, dla_main_data = fast_compute( c, graph_radius, dla_main_data )
    main_image.set_data(dla_main_data.T)

def trim_image(img):
    img = img[~np.all(img == 0, axis=1),:]
    img = img[:,~np.all(img == 0, axis=0)]
    return img

if False: #<- animation
    ani = animation.FuncAnimation(fig, animate, save_count=3000)
    # if there's an arg in command line, like `python dla.py v`, then produce the movie. otherwise, show animation.
    if len(sys.argv)>1:
        ani.save("DLA_%s.mp4"%(time.strftime("%Y%m%d_%H%M%S")), fps=100)
    else:
        plt.show()
else: #<- only calculation
    a = time.time()
    for j in range(1): #<- do the experiment 10 times, generate 10 pieces of data
        init()
        print(j,"round")
        for i in range(1):
            animate(0)
        plt.figure()
        plt.imshow(trim_image(dla_main_data.T), cmap='gray_r', origin='lower')
        plt.title("DLA with downward tendency of gravity")
        plt.savefig("data/dla_main_data_gravitation_%d_particles_%s.png"%(i+2, time.strftime("%Y%m%d_%H%M%S")))
        plt.close()
        with open("data/dla_main_data_gravitation_%d_particles_%s.pickle"%(i+2, time.strftime("%Y%m%d_%H%M%S")), "wb") as f: 
            pickle.dump(dla_main_data, f)
    b = time.time()
    print("total time: ", b-a)