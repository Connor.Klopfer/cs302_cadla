import numpy as np
import matplotlib.pyplot as plt

space_size = 5
space = np.zeros((space_size, space_size), dtype='b')

particles = np.random.rand(space_size,space_size)<0.1

#print(particles)

x = np.cumsum((-1)**(np.random.rand(10,2)<0.5), axis=0)

print(x)
plt.plot(x[:,0],x[:,1])
plt.show()