import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-g", "--growth_rate", help="increase output verbosity", type=float)
args = parser.parse_args()
if args.growth_rate:
    print("args.growth_rate = ",args.growth_rate)