import matplotlib. pyplot as plt
from matplotlib import animation, colors
import numpy as np
import time

pause = False
data = np.zeros((100,100),dtype='i')
y = 10
fig = plt.figure()
main_image = plt.imshow(data)
def onClick(event):
    global pause
    pause = ~pause
def data_func():
    global y
    y += 1
    if y>=100:
        y=0
def null_func():
    pass
def frame():
    while True:
        if ~pause:
            yield data_func()
        else:
            yield null_func()
def animate(args):
    if ~pause:
        print(y)
        new_data = data.copy()
        new_data[y,0] = 1
        main_image.set_data(new_data)

fig.canvas.mpl_connect('button_press_event', onClick)
animation = animation.FuncAnimation(fig, animate, frames=frame, save_count=200, repeat = False)
plt.show()