import numpy as np
import numpy
import scipy.ndimage
import matplotlib.pyplot as plt
from numba import jit


#@jit(nopython=True)
def test():
    a = np.zeros((50,100))
    a[3:20,2:50] = 1
    b = scipy.ndimage.binary_dilation(a, iterations=5).astype(a.dtype)
    c = scipy.ndimage.binary_dilation(b, iterations=5).astype(b.dtype)
    return a,b,c

a,b,c = test()
plt.subplot(2,2,1)
plt.imshow(a)
plt.subplot(2,2,2)
plt.imshow(b)
plt.subplot(2,2,3)
plt.imshow(c)

print(len(np.nonzero(c)[0]))

#plt.show()
