import numpy as np
import pylab as pl
import pickle

# read data from all 10001-particle results
fname = 'dla_main_data_10001_particles_20191001_081927.pickle'
f = open(fname, "rb")
dla_main_data = pickle.load(f)

print('dla shape: ', dla_main_data.shape)

def box_count(image_data, box_side):
    M = image_data.shape[0]

    if M % box_side == 0:
        # calculate the numnber of boxes on one side
        # the total nunber of boxes is (box_onside)^2
        box_onside = int((M / box_side))
    else:
        box_onside = int((M / box_side + 1))

    # a dictionary will store all boxes that contain images
    boxes = {}
    for a in range(box_onside):
        for b in range(box_onside):
            for i in range(a*box_side,box_side + a*box_side):
                for j in range(b*box_side,box_side + b*box_side):
                    try:
                        # finding all the non-zero pixels                       
                        if dla_main_data[i,j]>0:
                            if ((a,b) not in boxes):
                                boxes[(a,b)] = 1
                            else:
                                boxes[(a,b)] += 1
                    except:
                        pass
    #print(boxes)
    #print(len(boxes))
    box_num = len(boxes)
    return box_num 

def fractal_dimension(start_side, stop_side, side_interval):
    number_boxes = []
    one_divided_by_side_length = []
    # calculate the box number by using box_count() function
    for i in range(start_side, stop_side, side_interval):
        boxes = box_count(dla_main_data,i)
        number_boxes.append(boxes)
        one_divided_by_side_length.append(1.0/i)

    return number_boxes, one_divided_by_side_length

# retrun two lists of box number and 1/length of box
number_boxes, one_divided_by_side_length = fractal_dimension(1,11,1)
print(number_boxes)

# linear fit, polynomial of degree 1
coeffs=np.polyfit(np.log(one_divided_by_side_length), np.log(number_boxes), 1)
 
pl.plot(np.log(one_divided_by_side_length),np.log(number_boxes), 'o', mfc='none')
pl.plot(np.log(one_divided_by_side_length), np.polyval(coeffs,np.log(one_divided_by_side_length)))
pl.xlabel('log 1/length of side')
pl.ylabel('log N')
pl.savefig('box-counting.pdf')
pl.show()
 
print ("The fractal dimension is", coeffs[0]) #the fractal dimension is the slope of the line
 

